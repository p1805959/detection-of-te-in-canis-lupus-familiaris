#!/bin/bash

set -e
 
mkdir res/detect_te_on_refgen -p

echo
for file in $(cat ./data/file.list.txt); do
    echo "Detecting res/filter_reads_on_te/${file}.non_chimeric.fl.bam on genome"
    echo
    python3 src/TEatPos.py res/filter_reads_on_te/${file}.non_chimeric.fl.bam ./res/filter_reads_on_refgen/${file}.primary.bam -to res/detect_te_on_refgen/${file}.non_chimeric.fl
    echo "Done"
    echo 
    echo
done

# Making testis and cancer annotation

echo "Concatenating dog files to make a global for testis"
head -n 1 res/detect_te_on_refgen/aston.non_chimeric.fl.tsv > res/detect_te_on_refgen/testis.non_chimeric.fl.tsv
tail -qn +2 res/detect_te_on_refgen/aston.non_chimeric.fl.tsv res/detect_te_on_refgen/domino.non_chimeric.fl.tsv res/detect_te_on_refgen/emir.non_chimeric.fl.tsv res/detect_te_on_refgen/fute.non_chimeric.fl.tsv res/detect_te_on_refgen/samy.non_chimeric.fl.tsv res/detect_te_on_refgen/voyou.non_chimeric.fl.tsv >> res/detect_te_on_refgen/testis.non_chimeric.fl.tsv
cat res/detect_te_on_refgen/aston.non_chimeric.fl.bed res/detect_te_on_refgen/domino.non_chimeric.fl.bed res/detect_te_on_refgen/emir.non_chimeric.fl.bed res/detect_te_on_refgen/fute.non_chimeric.fl.bed res/detect_te_on_refgen/samy.non_chimeric.fl.bed res/detect_te_on_refgen/voyou.non_chimeric.fl.bed > res/detect_te_on_refgen/testis.non_chimeric.fl.bed

echo "Concatenating dog files to make a global for cancer"
head -n 1 res/detect_te_on_refgen/bear.non_chimeric.fl.tsv > res/detect_te_on_refgen/cancer.non_chimeric.fl.tsv
tail -qn +2 res/detect_te_on_refgen/bear.non_chimeric.fl.tsv res/detect_te_on_refgen/chipie.non_chimeric.fl.tsv res/detect_te_on_refgen/chrystalPoumon.non_chimeric.fl.tsv res/detect_te_on_refgen/chrystalRate.non_chimeric.fl.tsv res/detect_te_on_refgen/cml10.non_chimeric.fl.tsv res/detect_te_on_refgen/popsi.non_chimeric.fl.tsv res/detect_te_on_refgen/twiny.non_chimeric.fl.tsv res/detect_te_on_refgen/vico.non_chimeric.fl.tsv >> res/detect_te_on_refgen/cancer.non_chimeric.fl.tsv
cat res/detect_te_on_refgen/bear.non_chimeric.fl.bed res/detect_te_on_refgen/chipie.non_chimeric.fl.bed res/detect_te_on_refgen/chrystalPoumon.non_chimeric.fl.bed res/detect_te_on_refgen/chrystalRate.non_chimeric.fl.bed res/detect_te_on_refgen/cml10.non_chimeric.fl.bed res/detect_te_on_refgen/popsi.non_chimeric.fl.bed res/detect_te_on_refgen/twiny.non_chimeric.fl.bed res/detect_te_on_refgen/vico.non_chimeric.fl.bed > res/detect_te_on_refgen/cancer.non_chimeric.fl.bed
