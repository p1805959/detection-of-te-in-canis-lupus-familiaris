#!/bin/bash

set -e


mkdir res/filter_reads_on_refgen -p

echo
for file in $(cat ./data/file.list.txt); do
    echo "Filtering res/align_reads_on_refgen/${file}.bam"
    echo
    samtools view res/align_reads_on_refgen/${file}.bam -bF 0x100 -F 0x800 > "res/filter_reads_on_refgen/${file}.primary.bam"
    echo "Done"
    echo 
    echo
done