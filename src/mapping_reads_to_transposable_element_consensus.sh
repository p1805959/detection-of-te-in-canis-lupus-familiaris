#!/bin/bash

set -e

# align reads with a list of consensus TE
 
mkdir res/align_reads_on_te -p

echo
for file in $(cat ./data/file.list.txt); do
    echo "Mapping ./data/reads/${file}.fastq.gz on TE consensus"
    echo
    minimap2 -aYx splice data/transposons_consensus.fa data/reads/${file}.fastq.gz --sam-hit-only | samtools view -b > res/align_reads_on_te/${file}_reads_on_TE.bam
    echo "Done"
    echo 
    echo
done
