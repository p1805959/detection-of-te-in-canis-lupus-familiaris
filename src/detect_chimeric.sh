#!/bin/bash

set -e
 
mkdir res/detect_te_on_refgen -p

echo
for file in $(cat ./data/file.list.txt); do
    echo "Detecting res/filter_reads_on_te/${file}.chimeric.fl.bam on genome"
    echo
    python3 src/TEatPos.py res/filter_reads_on_te/${file}.chimeric.fl.bam ./res/filter_reads_on_refgen/${file}.primary.bam -to res/detect_te_on_refgen/${file}.chimeric.fl
    echo "Done"
    echo 
    echo
done

# Making testis and cancer annotation

echo "Concatenating dog files to make a global for testis"
head -n 1 res/detect_te_on_refgen/aston.chimeric.fl.tsv > res/detect_te_on_refgen/testis.chimeric.fl.tsv
tail -qn +2 res/detect_te_on_refgen/aston.chimeric.fl.tsv res/detect_te_on_refgen/domino.chimeric.fl.tsv res/detect_te_on_refgen/emir.chimeric.fl.tsv res/detect_te_on_refgen/fute.chimeric.fl.tsv res/detect_te_on_refgen/samy.chimeric.fl.tsv res/detect_te_on_refgen/voyou.chimeric.fl.tsv >> res/detect_te_on_refgen/testis.chimeric.fl.tsv
cat res/detect_te_on_refgen/aston.chimeric.fl.bed res/detect_te_on_refgen/domino.chimeric.fl.bed res/detect_te_on_refgen/emir.chimeric.fl.bed res/detect_te_on_refgen/fute.chimeric.fl.bed res/detect_te_on_refgen/samy.chimeric.fl.bed res/detect_te_on_refgen/voyou.chimeric.fl.bed > res/detect_te_on_refgen/testis.chimeric.fl.bed

echo "Concatenating dog files to make a global for cancer"
head -n 1 res/detect_te_on_refgen/bear.chimeric.fl.tsv > res/detect_te_on_refgen/cancer.chimeric.fl.tsv
tail -qn +2 res/detect_te_on_refgen/bear.chimeric.fl.tsv res/detect_te_on_refgen/chipie.chimeric.fl.tsv res/detect_te_on_refgen/chrystalPoumon.chimeric.fl.tsv res/detect_te_on_refgen/chrystalRate.chimeric.fl.tsv res/detect_te_on_refgen/cml10.chimeric.fl.tsv res/detect_te_on_refgen/popsi.chimeric.fl.tsv res/detect_te_on_refgen/twiny.chimeric.fl.tsv res/detect_te_on_refgen/vico.chimeric.fl.tsv >> res/detect_te_on_refgen/cancer.chimeric.fl.tsv
cat res/detect_te_on_refgen/bear.chimeric.fl.bed res/detect_te_on_refgen/chipie.chimeric.fl.bed res/detect_te_on_refgen/chrystalPoumon.chimeric.fl.bed res/detect_te_on_refgen/chrystalRate.chimeric.fl.bed res/detect_te_on_refgen/cml10.chimeric.fl.bed res/detect_te_on_refgen/popsi.chimeric.fl.bed res/detect_te_on_refgen/twiny.chimeric.fl.bed res/detect_te_on_refgen/vico.chimeric.fl.bed > res/detect_te_on_refgen/cancer.chimeric.fl.bed
