set -e
mkdir -p visualization


## Count by family

# Align

echo
echo "Count TE expression by family in step 'alignment' (after the mapping of reads on TE)"
printf "Count\tTE\tDog\n" > visualization/align.tsv
for dog in $(cat ./data/file.list.txt); do
    echo "Parsing res/align_reads_on_te/${dog}_reads_on_TE.bam"
    echo
    samtools view res/align_reads_on_te/${dog}_reads_on_TE.bam | awk '{print $3}' | sort | uniq -c | sed -E 's/^ *//; s/ /\t/' | sort -nrk 1 | sed "s/$/\t$dog/" >> visualization/align.tsv
    echo "Done"
    echo 
    echo
done


# Filter

echo "Count TE expression by family in step 'filter' (after the filtring of chimeric/non chimeric reads)"
printf "Count\tTE\tFilter\tDog\n" > visualization/filter.tsv
for dog in $(cat ./data/file.list.txt); do
    echo "Parsing res/filter_reads_on_te/${dog}.non_chimeric.fl.bam"
    samtools view res/filter_reads_on_te/${dog}.non_chimeric.fl.bam | awk '{print $3}' | sort | uniq -c | sed -E 's/^ *//; s/ /\t/' | sort -nrk 1 | sed "s/$/\tnon_chimeric.fl\t$dog/" >> visualization/filter.tsv
    echo "Parsing res/filter_reads_on_te/${dog}.chimeric.fl.bam"
    samtools view res/filter_reads_on_te/${dog}.chimeric.fl.bam | awk '{print $3}' | sort | uniq -c | sed -E 's/^ *//; s/ /\t/' | sort -nrk 1 | sed "s/$/\tchimeric.fl\t$dog/" >> visualization/filter.tsv   
    echo "Done"
    echo 
    echo
done


# Detect


echo "Count TE expression by family in step 'detect' (reads with TE and aligned on genome)"
printf "Count\tTE\tFilter\tDog\n" > visualization/detect.tsv
for dog in $(cat ./data/file.list.txt); do
    echo "Parsing res/detect_te_on_refgen/${dog}.non_chimeric.fl.bed"
    cat res/detect_te_on_refgen/${dog}.non_chimeric.fl.bed | awk '{print $4}' | sort | uniq -c | sed -E 's/^ *//; s/ /\t/'| sort -nrk 1 | sed "s/$/\tnon_chimeric.fl\t$dog/" >> visualization/detect.tsv
    echo "Parsing res/detect_te_on_refgen/${dog}.chimeric.fl.bed"
    cat res/detect_te_on_refgen/${dog}.chimeric.fl.bed | awk '{print $4}' | sort | uniq -c | sed -E 's/^ *//; s/ /\t/'| sort -nrk 1 | sed "s/$/\tchimeric.fl\t$dog/" >> visualization/detect.tsv
    echo "Done"
    echo 
    echo
done


## TE classification

# TE classification

printf "TE\tFamily\n" > visualization/TE_classification.tsv
cat data/transposons_consensus.fa | grep ">" | awk -F"\t" '{print substr($1,2)"\t"$2}' >> visualization/TE_classification.tsv

