#!/bin/bash

set -e

# align reads on reference genome
 
mkdir res/align_reads_on_refgen -p

echo
for file in $(cat ./data/file.list.txt); do
    echo "Mapping ./data/reads/${file}.fastq.gz on reference genome"
    echo
    minimap2 -ax splice data/reference_genome.fa data/reads/${file}.fastq.gz --sam-hit-only | samtools view -b > res/align_reads_on_refgen/${file}.bam
    echo "Done"
    echo 
    echo
done
