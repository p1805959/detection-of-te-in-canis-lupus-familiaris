#!/bin/bash

set -e
 
mkdir res/annot_te -p

echo
for file in $(cat ./data/file.list.txt); do
    echo "Annotating res/detect_te_on_refgen/${file}.chimeric.fl.tsv"
    echo
    python3 src/annotTE.py res/detect_te_on_refgen/${file}.chimeric.fl.tsv ./data/gene_annotation.gtf res/annot_te/${file}.chimeric.fl.annot.tsv
    echo "Done"
    echo 
    echo
done

# Making testis and cancer annotation

echo "Concatenating dog files to make a global for testis"
head -n 1 res/annot_te/aston.chimeric.fl.annot.tsv > res/annot_te/testis.chimeric.fl.annot.tsv
tail -qn +2 res/annot_te/aston.chimeric.fl.annot.tsv res/annot_te/domino.chimeric.fl.annot.tsv res/annot_te/emir.chimeric.fl.annot.tsv res/annot_te/fute.chimeric.fl.annot.tsv res/annot_te/samy.chimeric.fl.annot.tsv res/annot_te/voyou.chimeric.fl.annot.tsv >> res/annot_te/testis.chimeric.fl.annot.tsv

echo "Concatenating dog files to make a global for testis"
head -n 1 res/annot_te/bear.chimeric.fl.annot.tsv > res/annot_te/cancer.chimeric.fl.annot.tsv
tail -qn +2 res/annot_te/bear.chimeric.fl.annot.tsv res/annot_te/chipie.chimeric.fl.annot.tsv res/annot_te/chrystalPoumon.chimeric.fl.annot.tsv res/annot_te/chrystalRate.chimeric.fl.annot.tsv res/annot_te/cml10.chimeric.fl.annot.tsv res/annot_te/popsi.chimeric.fl.annot.tsv res/annot_te/twiny.chimeric.fl.annot.tsv res/annot_te/vico.chimeric.fl.annot.tsv >> res/annot_te/cancer.chimeric.fl.annot.tsv