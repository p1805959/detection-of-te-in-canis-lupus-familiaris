set -e

mkdir -p res/sorted_interval

for input in res/interval_te/*.tsv; do
    echo "Sorting ${input}"
    output=${input/"interval_te"/"sorted_interval"}
    head -n 1 "$input" > ${output%.tsv}.sorted.tsv
    tail -n +2 "$input" | sort -nrk 5 >> ${output%.tsv}.sorted.tsv
    echo "Sorted output in ${output%.tsv}.sorted.tsv"
    echo
done