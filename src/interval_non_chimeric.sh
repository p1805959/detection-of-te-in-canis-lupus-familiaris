#!/bin/bash

set -e
 
mkdir res/interval_te -p

echo
for file in $(cat ./data/file.list.txt); do
    echo "Merging TE in res/annot_te/${file}.non_chimeric.fl.annot.tsv"
    echo
    python3 ./src/interval_cluster.py res/annot_te/${file}.non_chimeric.fl.annot.tsv ./res/interval_te/${file}.non_chimeric.fl.interval -p 1000
    echo "Done"
    echo 
    echo
done


# Making testis and cancer intervals

echo "Merging TE in res/annot_te/testis.non_chimeric.fl.annot.tsv"
echo
python3 ./src/interval_cluster.py res/annot_te/testis.non_chimeric.fl.annot.tsv ./res/interval_te/testis.non_chimeric.fl.interval -p 1000
echo "Done"
echo
echo

echo "Merging TE in res/annot_te/cancer.non_chimeric.fl.annot.tsv"
echo
python3 ./src/interval_cluster.py res/annot_te/cancer.non_chimeric.fl.annot.tsv ./res/interval_te/cancer.non_chimeric.fl.interval -p 1000
echo "Done"
echo
echo