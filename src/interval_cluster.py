""" Clustering interval.
"""

import pandas as pd
import numpy as np
import argparse
from sklearn.cluster import DBSCAN 

# Shell interface

parser = argparse.ArgumentParser(
                    prog = 'intervalles.py',
                    description = 'From the precedent .tsv, create file describing new intervalles.',
                    epilog = 'For more information, contact pierre.gerenton@etu.univ-lyon1.fr.')

parser.add_argument("tsvpath",
                    help = "Path of a .tsv file.")
parser.add_argument("output_prefix", default=None,
                    help = "Prefix of the output file generated.")
parser.add_argument("-p","--dbscan_parameter",
                    help="DBSCAN's sklearn eps parameters",
                    required=False,
                    default=1000,
                    type=int)

args = parser.parse_args()

# read file

data = pd.read_csv(args.tsvpath, sep="\t")

#data = data[data["Chr"]!="*"]  # on enlève les reads qui ne se retrouve pas sur le génome
# did at previous step (annotation)

# data = data.astype({"Start":int, "End":int})

list_chr = list(np.unique(data["Chr"]))
list_dog = list(np.unique(data["Origin"]))

columns = ["Chr","Start","End","TE Name","Count","Strand","Length on chr","Indicator","Gene ID","Gene Biotype"]
columns.extend(list_dog)

interval_df = pd.DataFrame(columns=columns)

duree = len(list_chr)
i = 0

for chromosome in list_chr:
    i+=1
    if i%15==0:
        print(i, " / ",duree) 
    df = data[data["Chr"]==chromosome].copy()

    #clustering
    clf = DBSCAN(1000, min_samples=1)
    clf.fit(df.loc[:,['Start','End']])
    #df.loc[:,["label"]] = clf.labels_

    df_te = pd.DataFrame()
    df_te["Group"] = np.unique(clf.labels_)
    df_te["Chr"] = chromosome
    df_te["Start"] = df_te["Group"].apply(lambda x: min(df[clf.labels_==x]["Start"]))
    df_te["End"] = df_te["Group"].apply(lambda x: max(df[clf.labels_==x]["End"]))
    df_te["TE Name"] = df_te["Group"].apply(lambda x: max(set(df[clf.labels_==x]["TE Name/Fragment origin"]), key=list(df[clf.labels_==x]["TE Name/Fragment origin"]).count))
    df_te["Count"] = df_te["Group"].apply(lambda x: sum(clf.labels_==x))
    df_te["Strand"] = '*'
    df_te["Length on chr"] = df_te["Group"].apply(lambda x: np.mean(df[clf.labels_==x]["Length_on_chromosome"]))
    df_te["Indicator"] = df_te["Group"].apply(lambda x: max(set(df[clf.labels_==x]["Indicator"]), key=list(df[clf.labels_==x]["Indicator"]).count))
    df_te["Gene ID"] = df_te["Group"].apply(lambda x: max(set(df[clf.labels_==x]["gene_ID"]), key=list(df[clf.labels_==x]["gene_ID"]).count))
    df_te["Gene Biotype"] = df_te["Group"].apply(lambda x: max(set(df[clf.labels_==x]["gene_biotype"]), key=list(df[clf.labels_==x]["gene_biotype"]).count))
    for dog in list_dog:
        df_te[dog] = df_te["Group"].apply(lambda x: sum(df[clf.labels_==x]["Origin"] == dog))


    interval_df = pd.concat([interval_df,df_te.loc[:,columns].copy()])

    
interval_df.loc[interval_df["Start"]<0,["Start"]] = 0 # avoid bug
interval_df.loc[interval_df["End"]<0,["End"]] = 0 # avoid bug


# Output file

print("File output :")

interval_df.loc[:,["Chr","Start","End","TE Name","Count","Strand"]].to_csv(args.output_prefix + ".bed", header=False, index=False, sep="\t")
print(args.output_prefix + ".bed file of interval features")

interval_df.loc[:,columns].to_csv(args.output_prefix + ".tsv", index=False, sep="\t")
print(args.output_prefix + ".tsv file of interval features and info")
