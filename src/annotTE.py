"Annot TE.tsv list with gene id and gene biotype"

import argparse

# Shell interface

parser = argparse.ArgumentParser(
                    prog = 'annotTE.py',
                    description = 'Associate found TE with gene id and gene biotype . Return a tsv file.',
                    epilog = 'For more information, contact pierre.gerenton@etu.univ-lyon1.fr.')

parser.add_argument("TEtsv",
                    help = "Path of a TSV file with the position of TE")
parser.add_argument("annotation",
                    help = "Path of a GTF file with the an annotaton.\nShould have a gene biotype and a gene_id.")
parser.add_argument("output",
                    help="Output path")

args = parser.parse_args()


# Function

class Gene:
    """
    - Class inspired by Eric Cumenel
    """
    def __init__(self, chrom, start, end, gene_id, gene_biotype):
        self.chrom = chrom
        self.start = start
        self.end = end
        self.gene_id = gene_id
        self.gene_biotype = gene_biotype

    def __len__(self):
        return self.end - self.start

    def __repr__(self):
        return self.gene_id

def parse_gene_annotation_line(line):
    """
    - Function inspired by Eric Cumenel
    """
    sline = line.strip().split("\t")
    chrom = sline[0]
    start = int(sline[3])
    end = int(sline[4])
    info_list = sline[-1].split(";")
    for info in info_list:
        info = info.strip().split(" ")
        if info[0] == "gene_id" :
            gene_id = info[1][1:-1]
        elif info[0] == "gene_biotype" :
            gene_biotype = info[1][1:-1]
    return Gene(chrom, start, end, gene_id, gene_biotype)

def get_gene_dict(gene_annotation):
    """
    Convert an annotation file (gtf) of gene into a list of Gene.
    
    Args :
        gene_annotation (str) : path of the gene annotation (GFF format)

    - Function inspired by Eric Cumenel
    """
    gene_dict = {}
    with open(gene_annotation, 'r') as gene_annot:
        for line in gene_annot:
            sline = line.strip().split('\t')
            if sline[2] == "gene":
                new_gene = parse_gene_annotation_line(line)
                chrom = new_gene.chrom
                gene_id = new_gene.gene_id
                if chrom in gene_dict:
                    gene_dict[chrom].append(new_gene)
                else:
                    gene_dict[chrom] = [new_gene]
    return gene_dict

def is_overlapped(start1, end1, start2, end2):
    """
    - Function inspired by Eric Cumenel
    """
    return end1 >= start2 and end2 >= start1

def get_overlapped_genes(te_line, gene_dict):
    """
    Take a line (tsv) representing a TE and a gene list and return a list of
    all overlapping genes.
    
    Args:
        te_line (str)
        gene_dict (list of Gene)
        
    Returns:
        list(Gene)
        
    - Function inspired by Eric Cumenel
    """
    overlapped_genes = list()
    te_split = line.strip().split('\t')
    chrom = te_split[2]
    if chrom in gene_dict:
        te_start = int(te_split[3])
        te_end = int(te_split[4])
        for gene in gene_dict[chrom]:
            gene_start = gene.start
            gene_end = gene.end
            if is_overlapped(te_start, te_end, gene_start, gene_end):
                overlapped_genes.append(gene)
    return overlapped_genes

def intersection_of_intervals(start1, end1, start2, end2):
    """
    Interval HAS TO have an intersection (otherwise negative result may occur)
    """
    start = max(start1, start2)
    end = min(end1, end2)
    return end - start + 1

def union_of_intervals(start1, end1, start2, end2):
    """
    Interval HAS TO have an intersection (otherwise bigger number may occur)
    """
    start = min(start1, start2)
    end = max(end1, end2)
    return end - start + 1

def jacquard(start1, end1, start2, end2):
    """
    Interval HAS TO have an intersection or it don't work
    """
    return intersection_of_intervals(start1, end1, start2, end2) / union_of_intervals(start1, end1, start2, end2)

def get_best_gene(te_line, gene_list):
    """
    Take a line (tsv) representing a TE and a gene list and return a list of the best gene_id and gene_biotype
    Don't manage equality (use jacquard distance as score).

    Args:
        te_line (str)
        gene_dict (list of Gene) : needs to be > 1 !
        
    Returns:
        list(str) [gene_id, gene_biotype]e
    """
    te_split = te_line.strip().split('\t')
    te_start = int(te_split[3])
    te_end = int(te_split[4])
    best_gene = ["None","None"]
    max_score = 0
    for gene in gene_list:
        gene_start = gene.start
        gene_end = gene.end
        score = jacquard(gene_start, gene_end, te_start, te_end)
        if score > max_score:
            max_score = score
            best_gene = [gene.gene_id, gene.gene_biotype]

    return best_gene

# Main

if __name__ == "__main__":
    
    # parse annotation

    gene_dict = get_gene_dict(args.annotation)

    # parse TE

    with open(args.TEtsv, 'r') as tetsv:
        with open(args.output, 'w') as o:
            lines = tetsv.readlines()

            # print header

            o.write(lines[0].strip() + "\tgene_ID\tgene_biotype\n")
            # get best gene by lines and print

            for line in lines[1:]:
                if line.split('\t')[2] == '*': # skip unmapped read/TE
                    continue
                overlapped_genes = get_overlapped_genes(line, gene_dict)
                best_gene = get_best_gene(line, overlapped_genes)
                o.write(line.strip() + "\t" + best_gene[0] + "\t" + best_gene[1] + '\n')

