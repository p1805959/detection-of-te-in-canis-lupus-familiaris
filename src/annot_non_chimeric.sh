#!/bin/bash

set -e
 
mkdir res/annot_te -p

echo
for file in $(cat ./data/file.list.txt); do
    echo "Annotating res/detect_te_on_refgen/${file}.non_chimeric.fl.tsv"
    echo
    python3 src/annotTE.py res/detect_te_on_refgen/${file}.non_chimeric.fl.tsv ./data/gene_annotation.gtf res/annot_te/${file}.non_chimeric.fl.annot.tsv
    echo "Done"
    echo 
    echo
done

# Making testis and cancer annotation

echo "Concatenating dog files to make a global for testis"
head -n 1 res/annot_te/aston.non_chimeric.fl.annot.tsv > res/annot_te/testis.non_chimeric.fl.annot.tsv
tail -qn +2 res/annot_te/aston.non_chimeric.fl.annot.tsv res/annot_te/domino.non_chimeric.fl.annot.tsv res/annot_te/emir.non_chimeric.fl.annot.tsv res/annot_te/fute.non_chimeric.fl.annot.tsv res/annot_te/samy.non_chimeric.fl.annot.tsv res/annot_te/voyou.non_chimeric.fl.annot.tsv >> res/annot_te/testis.non_chimeric.fl.annot.tsv

echo "Concatenating dog files to make a global for testis"
head -n 1 res/annot_te/bear.non_chimeric.fl.annot.tsv > res/annot_te/cancer.non_chimeric.fl.annot.tsv
tail -qn +2 res/annot_te/bear.non_chimeric.fl.annot.tsv res/annot_te/chipie.non_chimeric.fl.annot.tsv res/annot_te/chrystalPoumon.non_chimeric.fl.annot.tsv res/annot_te/chrystalRate.non_chimeric.fl.annot.tsv res/annot_te/cml10.non_chimeric.fl.annot.tsv res/annot_te/popsi.non_chimeric.fl.annot.tsv res/annot_te/twiny.non_chimeric.fl.annot.tsv res/annot_te/vico.non_chimeric.fl.annot.tsv >> res/annot_te/cancer.non_chimeric.fl.annot.tsv