#!/bin/bash

set -e

 
mkdir res/filter_reads_on_te -p

echo
for file in $(cat ./data/file.list.txt); do
    echo "Filtering res/align_reads_on_te/${file}_reads_on_TE.bam"
    echo
    python3 src/filter_read.py res/align_reads_on_te/${file}_reads_on_TE.bam -q 0.85 -r 0.85 -o res/filter_reads_on_te/${file}.non_chimeric.fl.bam -k separate
    echo "Done"
    echo 
    echo
done
