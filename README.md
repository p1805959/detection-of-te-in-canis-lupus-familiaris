# Detection of TE in *Canis lupus familiaris* (dog) RNASeq long-read

This repository offer a method and a analysis of transposable element in RNASeq long-read data.

The depot is organized as follows:
- `./data` contains data or information on how to obtain it
- `./src` contains script to run the analysis
- `./res` will contains the middle and final results of the analysis

The detection and quantification of transposable element were already performed for :
- **non-chimeric** read : minimum 80% of the read and minimum 80% of the TE aligned
- **chimeric** read : maximum 50% of the read and minimum 80% of the TE aligned

If you want to know more about the origin of data, explore [`./data`](./data) and read [`./data/Data_Information.md`](./data/Data_Information.md).

If you want to explore those results without run the pipeline, explore [`./res`](./res) and read [`./res/Res_Information.md`](./res/Res_Information.md).

### Dependencies

- minimap2 == 2.26
- samtools == 1.18
- python3 >= 3.10.12
- R >= 4.3.1

Some python packages are necessary :

- pysam == 0.20.0
- pandas == 1.5.3
- numpy == 1.21.5
- sklearn == 1.2.1

And some R packages (for visualization) :

- dplyr == 1.1.3
- shiny == 1.7.5
- plotly == 4.10.2

### Help

This `README.md` is supposed to guide you to run the pipeline. You can obtain more help about the scripts by invoking them with the `-h` option.  \
Furthermore, you can contact `vincent.lacroix@univ-lyon1.fr` or `pierre.gerenton@etu.univ-lyon1.fr`.

## Summary

1. [Installation](#installation)
2. [Run scripts](#run-scripts)
    - [Mapping reads to reference genome](#mapping-reads-to-reference-genome)
    - [Filtring reads on reference genome alignment](#filtring-reads-on-reference-genome-alignment)
    - [Mapping reads to transposable element consensus](#mapping-reads-to-transposable-element-consensus)
    - [Filtring reads on transposable element consensus alignment](#filtring-reads-on-transposable-element-consensus-alignment)
    - [Detecting genomic position of TE](#detecting-genomic-position-of-te)
    - [Annotating found TE with a gene annotation](#annotating-found-te-with-a-gene-annotation)
    - [Quantification of transposons by loci using DBSCAN](#quantification-of-transposons-by-loci-using-dbscan)
    - [Sort intervals](#sort-intervals)
3. [Visualize output](#visualize-output)

## Installation

Clone the project in the folder of your choice :

```bash
git clone https://forge.univ-lyon1.fr/p1805959/detection-of-te-in-canis-lupus-familiaris.git
```

Then `cd` into the folder. You're now ready to begin ! Run each step in order and you'd be able to reproduce current result or obtain new ones with new parameters !

**Please note :** All necessary data aren't available in Git. If you want information about how to download reads or how other datafile were obtained, please refer to [`./data/Data_Information.md`](./data/Data_Information.md).


## Run scripts

### Mapping reads to reference genome

```sh
./src/mapping_reads_to_refgen.sh
```

Reads are mapped on the reference genome.
The generated file is `./data/bam/[dog_name].bam` in `./res/align_reads_on_refgen`

**Details :**

The first step is the alignment, with minimap2, of reads on reference consensus with `minimap2 -ax splice data/transposons_consensus.fa data/long_read/testicle/[dog_name].fastq.gz --sam-hit-only`. Then the file is convert to bam with samtools.


### Filtring reads on reference genome alignment

```sh
./src/filter_bam_primary.sh
```

To get more pertinant values, only primary alignment for the alignment file between the reads and the reference genome is maintained. 
The bash script will use samtools to filter every alignment with the flag "non primary" or "supplementary".
The output files are in `./res/filter_reads_on_refgen`.

### Mapping reads to transposable element consensus

```sh
./src/mapping_reads_to_transposable_element_consensus.sh 
```

Only reads which align with a TE consensus are kept.
The generated file is `./res/align_reads_on_te/[dog_name]_reads_on_TE.bam`.

**Details :**

The first step is the alignment, with minimap2, of reads on reference consensus with `minimap2 -ax splice data/transposons_consensus.fa data/long_read/testicle/[dog_name].fastq.gz --sam-hit-only`. Then the file is convert to bam with samtools.


### Filtring reads on transposable element consensus alignment

```sh
./src/filter_non_chimeric_read.sh 
./src/filter_chimeric_read.sh
```

This step consists of filtring reads to keep ones we want to study.

`keep_non_chimeric_read` will keep full-length and non chimeric read, in other worlds reads with the full sequence of the TE and that's all
`keep_chimeric_read` will keep full-length and chimeric read, in other worlds reads with the full sequence of the TE and transcript with other nucleic material.
.

Two files will be produced for each dogs (in `./res/filter_reads_on_te`):

- `[dog_name].non_chimeric.fl.bam` for full-length non-chimeric transcripts
- `[dog_name].chimeric.fl.bam` for chimeric transcripts

**Details :**

The python script `filter_read.py` is called for each conditions.

- **For the non-chimeric condition** : the query (read) coverage is set to be > 85% and reference (TE) coverage is set to be > 85% too. 
- **For the chimeric condition** : the query (read) coverage is set to be < 50% and reference (TE) coverage is set to be > 80% too. 

Also, if a read mapped on multiple TE, the script prevent overlapping TE to stay. The primary alignment will always be kept, and secondary alignments will be arbitrarily chosen as long as there is no overlap. Multiple other variant are implemented to keep *only primary* alignment, *all* alignments, or *primary and secondary* aligments (no supplumentary).



If you want to play with those threesold or parameters, you can run the `filter_read.py` differently (use `-h` option to know more about options).

### Detecting genomic position of TE

```sh
./src/detect_non_chimeric.sh
./src/detect_chimeric.sh
```

These script will infer the TE position in the genome throught the position of the TE in the read and the position of the read in the genome.

Two output file are generated :

- `res/detect_te/[dog_name].[chimeric.fl/non_chimeric.fl].bed` : bed6 file with the chromosomic position, the name of the TE and an "indicator score" (cf. Details)
- `res/detect_te/[dog_name].[chimeric.fl/non_chimeric.fl].tab` : optional output in tsv format more informative.

Also, the corresponding files are merged to create files representing the testis and cancer conditions.

**Details**:

The python script `TEatPos.py` is run with `-t` to also write the tabular output. The script will infer the position of the TE in the genome with the position of the TE in the read and the position of the read in the genome. To correctly find the position, the CIGAR string of the alignments are parsed. However, if the position of a TE is include within a soft-masked part of the alignment, the infered position is purely speculative.

That's why some indicator score is given :

- **0** : the genomic position is unaviable
- **1** : the genomic position are include in the read2genome alignment
- **2** : the end pos is included but the start wasn't
- **3** : the start pos is included but the end wasn't
- **4** : the start and end pos is in the soft-clipping part at the left of the cigarstring
- **5** : the start and the end is in the soft-clipping part at the right of the cigarstring
- **6** : the alignment is include in the TE

In addition, the `.tsv` file replace the empty column of the bed file by 3 alternative ones :

- The **read id** which support the TE
- The **Length_on_chromosome**, the number of base which match/mismatch the genome in the alignment
- The **origin** file


### Annotating found TE with a gene annotation

```sh
./src/annot_non_chimeric.sh
./src/annot_chimeric.sh
```

These script will annotate the TE position in the genome (in the previous `.tsv` file) thanks to a gene annotation.

The output file are generated in `res/detect_te/[dog_name].[chimeric.fl/non_chimeric.fl].annot.tsv`. 

Also, the corresponding files are merged to create files representing the testis and cancer conditions.


**Details** : 

The python script `annotTE.py` is run. For each TE, if it overlaps a gene annotation, the ID and biotype of the gene are noted. If a TE overlaps multiple gene annotation, the closest is chosen using Jaccard Distance.


### Quantification of transposons by loci using DBSCAN

```sh
./src/interval_non_chimeric.sh
./src/interval_chimeric.sh
```

Now, the objective is to find the loci with the most TE expressed.

The chosen approach is a clustering method named *DBSCAN*.

The method is used for each dog, the testis and the cancer condition.

Expected output files are :

- `[dog_name].[chimeric.fl/non_chimeric.fl].interval.bed` : found interval in bed format
- `[dog_name].[chimeric.fl/non_chimeric.fl].interval.tsv` : supplementary file to trace back the read expressing the TE of these interval and some annotation


**Details** :


The method uses the sklearn packages with the DBSCAN method. It will look for high-density loci and expand clusters from there. The maximum distance between two TE to be considered as close can be changed. This is the `-p` option of the python script (1000 by default).

### Sort intervals

```sh
./src/sort_intervals.sh
```

You can run this convenient script to sort intervals by number of TE expressed.

Outputs files are in `res/sorted_interval/[dog_name].[chimeric.fl/non_chimeric.fl].interval.sorted.tsv`.

## Visualize output

```sh
./src/prepare_visualization.sh
```

Run the script above to genere necessary file to visualize the TE repartition on the RShiny.

To run the RShiny App, execute :
```sh
Rscript src/see_TE_repartition.R
```
then you can open `http://127.0.0.1:XXXX` in a browser.
