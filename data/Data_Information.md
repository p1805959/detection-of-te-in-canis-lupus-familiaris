# Data origin and preprocessing

This document will explain where to find data for the analysis.

**Please note :** All data, except for heavy file as `.sam/.bam` (alignment) or `.fastq` (fastq) or legally protected data as the consensus for TE (from Repbase Update) are aviable in the repository.

## Reads

Reads has to be in [`./reads`](./reads) in `fastq.gz` format.
TODO

## Reference genome

The reference genome is the **canFam4** assembly. 
It is accessible with the accession number **GCF_011100685.1**. However, the copy present in the repository come from GenOuest and have different sequence ID.

ORIGIN ? TODO

## Alignment file

Alignment file come from GenOuest. It is the alignment of the reads against the reference genome.

## Gene Annotation

TODO

## TE Consensus

The TE Sequence consensus file was requested on RepBase Update to get the consensus for all TE present in *Canis lupus familiaris* or highter clades.

INFO ABOUT THE CORRECT GENERATION

## TE Annotation (for visualisation)

The University of Californa Santa Cruz (UCSC) database offer multiple annotation file including one contening repetition. The current file was generated at https://genome.ucsc.edu/cgi-bin/hgTables July 4, 2023.